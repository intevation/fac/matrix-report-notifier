# Element Report Notifier

This script notifies via e-mail a group of contacts about reported events (messages) of a [Synapse](https://github.com/matrix-org/synapse/) Matrix Server.

The recipients are all server admins of that server.

## Requirements

- `python3-psycopg`

## Configuration

Create a file `/etc/matrix-report-notifier.ini` with the following content:

```ini
[DEFAULT]
# Is shown as body when the message is encrypted
text_encrypted_message = Encrypted message.
text_private_room = Private room
strip_servername = off
# % sign needs to be escaped in this file format
date_format = %%d.%%m.%%Y %%H:%%M:%%S
template = Dear admins,

    An event on your Matrix server has been reported

    Reported event:
    Message: {body}
    Sender: {sender}
    Room: {room_alias}
    Sent at: {sent}

    Reporter: {reporter}
    Reason: {reason}
    Reported at: {reported}

    View report at http://example.com/synapse-admin/#/reports/{report_id}/show/detail

    This is an automated message by the matrix-report-notifier

[smtp]
from =
subject = Reported event from {sender}
host =

[database]
hostname =
username =
password =
database =
```

The template and subject can use formatting.

## Calling the script

Configure some mechanism to run the script regularly.

E.g. using cron, starting the script every five minutes:
```
# matrix-report-notifier
*/5 *  *   *   *     /opt/matrix-report-notifier/matrix-report-notifier.py
```

## Why via database and not via the API

Why does the script query the data from the database and not via the [REST API](https://matrix-org.github.io/synapse/latest/admin_api/event_reports.html)?
The latter requires an access token to be configured, which can change (e.g. on logout with a client) and is thus more unstable in daily operations.
Connecting to the database is also much faster, which makes the whole process more lightweight.
