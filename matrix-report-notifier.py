#!/usr/bin/python3
# https://matrix-org.github.io/synapse/latest/admin_api/event_reports.html
import psycopg2
import psycopg2.extras
import configparser
from email.message import EmailMessage
from smtplib import SMTP
import datetime

from typing import List

CONFIG = configparser.ConfigParser()
CONFIG.read('/etc/matrix-report-notifier.ini')
STATE_PATH = '/var/lib/matrix-report-notifier.txt'


def strip_server(string: str) -> str:
    if not isinstance(string, str):
        return string
    if CONFIG['DEFAULT'].getboolean('strip_servername'):
        return string[:string.find(':')]
    else:
        return string


try:
    with open(STATE_PATH) as handle:
        highest_id = int(handle.read().strip())
except FileNotFoundError:
    highest_id = -1


with psycopg2.connect(host=CONFIG["database"]["hostname"],
                      dbname=CONFIG["database"]["database"],
                      user=CONFIG["database"]["username"],
                      password=CONFIG["database"]["password"]) as conn:
    with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
        cur.execute("""SELECT
                    id, received_ts, user_id, room_id, reason, room_alias,
                    json::json->'sender' AS sender,
                    json::json->'origin_server_ts' AS origin_server_ts,
                    json::json->'content'->'body' AS body
                    FROM event_reports
                    NATURAL LEFT JOIN room_aliases
                    NATURAL JOIN event_json
                    WHERE id > %s;""",
                    (highest_id, ))
        report_list: List[dict] = cur.fetchall()

        cur.execute("""
                    SELECT user_threepids.address
                    FROM users
                    JOIN user_threepids ON users.name = user_threepids.user_id
                    WHERE
                    medium = 'email' AND
                    users.admin = 1;""")
        recipients: List[str] = [x['address'] for x in cur.fetchall()]

        for report in report_list:
            report_id = report['id']
            room_id = strip_server(report["room_id"])
            room_alias = strip_server(report["room_alias"])
            # if room is private, the room alias is null
            room_name = room_alias if room_alias is not None else f"CONFIG['DEFAULT']['text_private_room'] (ID: {room_id})"

            sender = strip_server(report['sender'])
            reported = datetime.datetime.fromtimestamp(report['received_ts']//1000).strftime(CONFIG['DEFAULT']['date_format'])
            reporter = strip_server(report['user_id'])
            reason = report['reason']
            sent = datetime.datetime.fromtimestamp(report['origin_server_ts']//1000).strftime(CONFIG['DEFAULT']['date_format'])
            # if message was encrypted, the value is null
            body = report['body'] if report['body'] is not None else CONFIG['DEFAULT']['text_encrypted_message']

            msg = EmailMessage()
            msg['Subject'] = CONFIG['smtp']['subject'].format(**locals())
            msg['From'] = CONFIG['smtp']['from']
            msg['To'] = recipients
            msg.set_content(CONFIG['DEFAULT']['template'].format(**locals()))

            with SMTP(CONFIG['smtp']['host']) as smtp:
                smtp.send_message(msg)

            with open(STATE_PATH, 'w') as handle:
                handle.write(f'{report_id}')
